import React from "react";
import Form from "./From";

export default function App() {
  return (
    <>
      <h1>Inscription</h1>
      <Form />
    </>
  );
}
