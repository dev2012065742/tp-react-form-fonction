import React, { useState } from "react";

export default function Form() {
  const [id, setId] = useState();
  const [password, setPassword] = useState();
  const [dataNaissance, setDateNaissance] = useState();
  const [ville, setVille] = useState();
  const [genre, setGenre] = useState();
  const [loisirs, setLoisirs] = useState([]);
  const [submitted, setSubmitted] = useState(false);

  const handleGenre = (e) => {
    setGenre(e.target.value);
  };

  const handleLoisirs = (e) => {
    const value = e.target.value;
    if (loisirs.includes(value)) {
      setLoisirs(loisirs.filter((item) => item !== value));
    } else {
      setLoisirs([...loisirs, value]);
    }
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    setSubmitted(true);
  };

  return (
    <>
      <form action="#" onSubmit={handleSubmit}>
        <label htmlFor="identifiant">L'identifiant</label>
        <input
          type="number"
          name="identifiant"
          id="identifiant"
          value={id}
          onChange={(e) => {
            setId(e.target.value);
          }}
        />
        <br />
        <label htmlFor="motDePasse">Mot de passe</label>
        <input
          type="password"
          name="motDePasse"
          id="motDePasse"
          value={password}
          onChange={(e) => {
            setPassword(e.target.value);
          }}
        />
        <br />
        <label htmlFor="dateNaissance">Date de naissance</label>
        <input
          type="date"
          name="dateNaissance"
          id="dateNaissance"
          value={dataNaissance}
          onChange={(e) => {
            setDateNaissance(e.target.value);
          }}
        />
        <br />
        <label htmlFor="ville">Ville</label>
        <select
          name="ville"
          id="ville"
          value={ville}
          onChange={(e) => {
            setVille(e.target.value);
          }}
        >
          <option value="casablanca">Casablanca</option>
          <option value="rabat">Rabat</option>
          <option value="tangier">Tangier</option>
        </select>
        <br />
        <label htmlFor="genre">Genre</label>
        <input
          type="radio"
          name="genre"
          id="genre"
          value="M"
          checked={genre === "M"}
          onChange={handleGenre}
        />
        Homme
        <input
          type="radio"
          name="genre"
          id="genre"
          value="F"
          checked={genre === "F"}
          onChange={handleGenre}
        />
        Femme
        <br />
        <label htmlFor="loisirs">Loisirs</label>
        <input
          type="checkbox"
          name="loisirs"
          value="sport"
          checked={loisirs.includes("sport")}
          onChange={handleLoisirs}
        />
        Sport
        <input
          type="checkbox"
          name="loisirs"
          value="lecture"
          checked={loisirs.includes("lecture")}
          onChange={handleLoisirs}
        />
        Lecture
        <input
          type="checkbox"
          name="loisirs"
          value="musique"
          checked={loisirs.includes("musique")}
          onChange={handleLoisirs}
        />
        Musique
        <br />
        <input type="submit" value="S'inscrire" />
      </form>
      {submitted && (
        <p>
          Je suis {id} né le {dataNaissance} à {ville} et mes loisirs sont :
          {loisirs}
        </p>
      )}
    </>
  );
}
